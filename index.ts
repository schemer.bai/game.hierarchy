// this is a event package
import * as _ from "lodash"

export type ISymbol = string | number

export class HierarchyMapError extends Error {}

export class HierarchyMap {

  private c2pMap: { [child: string]: ISymbol[] } = { t: ["t"] }
  private p2cMap: { [parent: string]: ISymbol[] } = {}

  constructor(list?: [ISymbol, ISymbol[]][]) {
    list && this.createWithList(list)
  }

  private _deriveHelper(child: ISymbol, parents: ISymbol[], isParentsAtSameLevel: boolean) {

    this.c2pMap[child] = [...parents, child]

    if (isParentsAtSameLevel) {
      _.each(parents, p => {
        if (!this.p2cMap[p]) this.p2cMap[p] = []
        this.p2cMap[p].push(child)
      })
    }
    else {
      let p = _.last(parents)
      if (p) {
        if (!this.p2cMap[p]) this.p2cMap[p] = []
        this.p2cMap[p].push(child)
      }
    }
  }

  derive(child: ISymbol, parents: ISymbol | ISymbol[] = "t") {
    if (this.c2pMap[child]) throw new HierarchyMapError(`derive: Tag ${child} is already exist in this HierarchyMap`)
    if (_.isString(parents) || _.isNumber(parents)) {
      if (!this.c2pMap[parents]) throw new HierarchyMapError(`derive: Do not find the Parent ${parents}`)
      this._deriveHelper(child, this.c2pMap[parents], false)
    }
    else if (_.isArray(parents)) {
      if (parents.length === 0) {
        this.derive(child)
      }
      else {
        let eachParentsList = _.map(parents, v => {
          if (!this.c2pMap[v]) throw new HierarchyMapError(`derive: Do not find the Parent ${v}`)
          else return this.c2pMap[v]
        })
        let ps = _.uniq(_.flatMap(eachParentsList))
        this._deriveHelper(child, ps, true)
      }
    }
    else {
      throw new HierarchyMapError(`the input parent ${parents} is not expected`)
    }
    return this
  }

  deriveFrom(parent: ISymbol, children: ISymbol[]) {
    _.each(children, child => this.derive(child, parent))
  }

  createWithList(lst: [ISymbol, ISymbol[]][]) {
    if (!_.isArray(lst)) throw new HierarchyMapError("create HierarchyMap with list: input is not a list")
    _.each(lst, item => {
      if (_.isArray(item)) {
        let child = item[0]
        let parents = item[1]
        if (child && parents)
          this.derive(child, parents)
      }
    })
  }

  is(child: ISymbol, parent: ISymbol) {
    if (this.c2pMap[child] && this.c2pMap[parent]) {
      if (child === parent) return true
      return this.c2pMap[child].indexOf(parent) >= 0
    }
    else {
      throw new HierarchyMapError(`do not find the symbol ${child}, ${parent}`)
    }
  }

  parents(child: ISymbol) {
    return this.c2pMap[child]
  }

  children(parent: ISymbol) {
    return this.p2cMap[parent]
  }
}