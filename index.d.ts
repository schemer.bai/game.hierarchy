export declare type ISymbol = string | number;
export declare class HierarchyMapError extends Error {
}
export declare class HierarchyMap {
    private c2pMap;
    private p2cMap;
    constructor(list?: [ISymbol, ISymbol[]][]);
    private _deriveHelper(child, parents, isParentsAtSameLevel);
    derive(child: ISymbol, parents?: ISymbol | ISymbol[]): this;
    deriveFrom(parent: ISymbol, children: ISymbol[]): void;
    createWithList(lst: [ISymbol, ISymbol[]][]): void;
    is(child: ISymbol, parent: ISymbol): boolean;
    parents(child: ISymbol): (string | number)[];
    children(parent: ISymbol): (string | number)[];
}
