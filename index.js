"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// this is a event package
const _ = require("lodash");
class HierarchyMapError extends Error {
}
exports.HierarchyMapError = HierarchyMapError;
class HierarchyMap {
    constructor(list) {
        this.c2pMap = { t: ["t"] };
        this.p2cMap = {};
        list && this.createWithList(list);
    }
    _deriveHelper(child, parents, isParentsAtSameLevel) {
        this.c2pMap[child] = [...parents, child];
        if (isParentsAtSameLevel) {
            _.each(parents, p => {
                if (!this.p2cMap[p])
                    this.p2cMap[p] = [];
                this.p2cMap[p].push(child);
            });
        }
        else {
            let p = _.last(parents);
            if (p) {
                if (!this.p2cMap[p])
                    this.p2cMap[p] = [];
                this.p2cMap[p].push(child);
            }
        }
    }
    derive(child, parents = "t") {
        if (this.c2pMap[child])
            throw new HierarchyMapError(`derive: Tag ${child} is already exist in this HierarchyMap`);
        if (_.isString(parents) || _.isNumber(parents)) {
            if (!this.c2pMap[parents])
                throw new HierarchyMapError(`derive: Do not find the Parent ${parents}`);
            this._deriveHelper(child, this.c2pMap[parents], false);
        }
        else if (_.isArray(parents)) {
            if (parents.length === 0) {
                this.derive(child);
            }
            else {
                let eachParentsList = _.map(parents, v => {
                    if (!this.c2pMap[v])
                        throw new HierarchyMapError(`derive: Do not find the Parent ${v}`);
                    else
                        return this.c2pMap[v];
                });
                let ps = _.uniq(_.flatMap(eachParentsList));
                this._deriveHelper(child, ps, true);
            }
        }
        else {
            throw new HierarchyMapError(`the input parent ${parents} is not expected`);
        }
        return this;
    }
    deriveFrom(parent, children) {
        _.each(children, child => this.derive(child, parent));
    }
    createWithList(lst) {
        if (!_.isArray(lst))
            throw new HierarchyMapError("create HierarchyMap with list: input is not a list");
        _.each(lst, item => {
            if (_.isArray(item)) {
                let child = item[0];
                let parents = item[1];
                if (child && parents)
                    this.derive(child, parents);
            }
        });
    }
    is(child, parent) {
        if (this.c2pMap[child] && this.c2pMap[parent]) {
            if (child === parent)
                return true;
            return this.c2pMap[child].indexOf(parent) >= 0;
        }
        else {
            throw new HierarchyMapError(`do not find the symbol ${child}, ${parent}`);
        }
    }
    parents(child) {
        return this.c2pMap[child];
    }
    children(parent) {
        return this.p2cMap[parent];
    }
}
exports.HierarchyMap = HierarchyMap;
